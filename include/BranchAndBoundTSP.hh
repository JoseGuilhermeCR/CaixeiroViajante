/**
 * @file BranchAndBoundTSP.hh
 * @author José Guilherme de C. Rodrigues
 * @version 1.0
 * @date 2021-05-06
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#pragma once

#include <cstdint>
#include <ostream>
#include <vector>

namespace BranchAndBoundSolution {

struct Coord {
    uint32_t x;
    uint32_t y;

    double distance(const Coord &other) const noexcept;

    friend std::ostream &operator<<(std::ostream &os, const Coord &coord);
};

std::pair<double, std::vector<uint32_t>> TravellingSalesPerson(uint32_t n, const std::vector<double> &adjacency_matrix);

}