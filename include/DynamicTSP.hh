/**
 * @file DynamicTSP.hh
 * @author José Guilherme C. Rodrigues
 * @version 1.0
 * @date 2021-05-06
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#pragma once

#include <vector>
#include <cstdint>

namespace DynamicSolution {

class Graph {
    public:
        Graph(uint32_t size);

        void AddPos(uint32_t n, double x, double y);
        void CreateEdges();

        std::pair<double, std::vector<uint32_t>> OPT(uint32_t v, const std::vector<uint32_t> &sub_set);
    private:
        uint32_t m_size;
        std::vector<std::vector<double>> m_matrix;
        std::vector<std::pair<double, double>> m_pos;
};

}