/**
 * @file BruteForceTSP.hh
 * @author José Guilherme de C. Rodrigues
 * @version 1.0
 * @date 2021-05-06
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#pragma once

#include <cstdint>
#include <vector>
#include <tuple>

namespace BruteForceSolution {

class Point {
    public:
        Point() noexcept;
        Point(uint32_t x, uint32_t y) noexcept;

        static double distance(const Point &u, const Point &v) noexcept;

        uint32_t x;
        uint32_t y;
};

void InitCidades(uint32_t n, const std::vector<double> &xs, const std::vector<double> &ys);
void InitDist(std::vector<std::vector<double>> &matrix, uint32_t n);
std::pair<std::vector<uint32_t>, double> TSP(const std::vector<std::vector<double>> &matrix);

}
