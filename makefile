# Made by José Guilherme de C. Rodrigues

IDIR=./include

CXX=g++

ifdef DEFS
CXXFLAGS=-Wall -Wextra -std=c++2a -pedantic -I$(IDIR) -D$(DEFS) -O3
LDFLAGS=-lm -lpthread
else
CXXFLAGS=-Wall -Wextra -std=c++2a -pedantic -I$(IDIR) -O3
LDFLAGS=-lm
endif

src=$(wildcard src/*.cc)
obj=$(src:.cc=.o)

tsp.out: $(obj)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LDFLAGS)

.PHONY: clean
clean:
	rm -f src/*.o tsp.out
