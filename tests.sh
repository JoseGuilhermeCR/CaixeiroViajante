#!/bin/bash

# Made by José Guilherme de C. Rodrigues

uname_out="$(uname -s)"
case "$uname_out" in
    Linux*)     echo "Platform is ok.";;
    Darwin*)    echo "Platform is probably ok.";;
    *)          echo "Can't guarantee that anything will run at all."; exit
esac

# Check if executable doesn't exist.
# If so, we build it.
if [ ! -x tsp.out ]
then
    echo "Building executable."
    make -j DEFS=THREADED
fi

if [ -d output_files ]
then
    # Delete every output file.
    rm -f output_files/*
else
    mkdir output_files
fi

# Check how many input files exist.
input_count=$(($(ls -l input_files | wc -l)-1))

# For each input file.
i=0
while [ $i -ne $input_count ]
do
    echo "Executing input number $i"

    original="output_files/$i.out"
    filtered="output_files/filtered_$i.out"

    # Run the program 10 times for each input file.
    # C-Style for just because.
    for ((test = 0 ; test != 10 ; ++test))
    do
        echo -e "\nTest number $test\n" >> $original
        ./tsp.out < input_files/$i.in >> $original
    done

    # Filter the output so it's easier to calculate the mean time.
    echo "Filtering input number $i"

    echo "Branch and Bound times: " > $filtered
    grep seconds $original | sort | head -n 10 | cut -d ' ' -f 7 >> $filtered

    echo "Brute Force times: " >> $filtered
    grep seconds $original | sort | tail -n +11 | head -n 10 | cut -d ' ' -f 6 >> $filtered

    echo "Dynamic times: " >> $filtered
    grep seconds $original | sort | tail -n +21 | head -n 10 | cut -d ' ' -f 5 >> $filtered

    echo "Program times: " >> $filtered
    grep seconds $original | sort | tail | cut -d ' ' -f 4 >> $filtered

    sed -i 's/$/+/' $filtered

    i=$((($i)+1))
done

