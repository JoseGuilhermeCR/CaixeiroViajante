/**
 * @file BranchAndBoundTSP.cc
 * @author José Guilherme de C. Rodrigues
 * @version 1.0
 * @date 2021-05-06
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <BranchAndBoundTSP.hh>
#include <cmath>
#include <limits>

namespace BranchAndBoundSolution {

double Coord::distance(const Coord &other) const noexcept {
  return std::sqrt((x - other.x) * (x - other.x) +
                   (y - other.y) * (y - other.y));
}

std::ostream &operator<<(std::ostream &os, const Coord &coord) {
  return os << '(' << coord.x << ',' << coord.y << ')';
}

namespace {
template <typename T>
const T &at(uint64_t n, const std::vector<T> &vec, uint64_t row, uint64_t col) {
  return vec.at(row * n + col);
}

double MinimumEdgeCost(uint32_t n, const std::vector<double> &adjacency_matrix,
                       uint32_t v) {
  auto min = std::numeric_limits<double>::infinity();
  for (uint32_t u = 0; u != n; ++u) {
    const auto value = at(n, adjacency_matrix, v, u);
    if (value < min && v != u) {
      min = value;
    }
  }

  return min;
}

double SecondMinimumEdgeCost(uint32_t n,
                             const std::vector<double> &adjacency_matrix,
                             uint32_t v) {
  auto first_min = std::numeric_limits<double>::infinity();
  auto second_min = first_min;

  for (uint32_t u = 0; u != n; ++u) {
    if (u == v) {
      continue;
    }

    const auto value = at(n, adjacency_matrix, v, u);
    if (value <= first_min) {
      second_min = first_min;
      first_min = value;
    } else if (value <= second_min && value != first_min) {
      second_min = value;
    }
  }

  return second_min;
}

void TravellingSalesPersonImpl(uint32_t n,
                               const std::vector<double> &adjacency_matrix,
                               double current_bound, double current_weight,
                               uint32_t level, std::vector<bool> &visited,
                               std::vector<uint32_t> &current_path,
                               double &final_res,
                               std::vector<uint32_t> &final_path) {
  if (level == n) {
    const auto value =
        at(n, adjacency_matrix, current_path[level - 1], current_path[0]);
    if (value != 0.0) {
      auto current_res = current_weight + value;

      if (current_res < final_res) {
        final_path = current_path;
        final_res = current_res;
      }
    }

    return;
  }

  for (uint32_t u = 0; u != n; ++u) {
    const auto value = at(n, adjacency_matrix, current_path[level - 1], u);
    if (value != 0 && !visited[u]) {
      auto tmp = current_bound;
      current_weight += value;

      if (level == 1) {
        current_bound -=
            (MinimumEdgeCost(n, adjacency_matrix, current_path[level - 1]) +
             MinimumEdgeCost(n, adjacency_matrix, u)) /
            2.0;
      } else {
        current_bound -=
            (MinimumEdgeCost(n, adjacency_matrix, current_path[level - 1]) +
             SecondMinimumEdgeCost(n, adjacency_matrix, u)) /
            2.0;
      }

      if (current_bound + current_weight < final_res) {
        current_path[level] = u;
        visited[u] = true;

        TravellingSalesPersonImpl(n, adjacency_matrix, current_bound,
                                  current_weight, level + 1, visited,
                                  current_path, final_res, final_path);
      }

      current_weight -= value;
      current_bound = tmp;

      for (uint32_t v = 0; v != visited.size(); ++v) {
        visited[v] = false;
      }

      for (uint32_t v = 0; v <= level - 1; ++v) {
        visited[current_path[v]] = true;
      }
    }
  }
}
} // namespace

std::pair<double, std::vector<uint32_t>>
TravellingSalesPerson(uint32_t n, const std::vector<double> &adjacency_matrix) {
  std::vector<uint32_t> current_path(n + 1,
                                     std::numeric_limits<uint32_t>::max());
  std::vector<bool> visited(n + 1, false);

  auto current_bound = 0;
  for (uint32_t u = 0; u != n; ++u) {
    current_bound += MinimumEdgeCost(n, adjacency_matrix, u) +
                     SecondMinimumEdgeCost(n, adjacency_matrix, u);
  }

  current_bound = current_bound & 1 ? current_bound / 2 + 1 : current_bound / 2;

  visited[0] = true;
  current_path[0] = 0;

  auto final_res = std::numeric_limits<double>::infinity();
  std::vector<uint32_t> final_path;
  TravellingSalesPersonImpl(n, adjacency_matrix,
                            static_cast<double>(current_bound), 0.0, 1, visited,
                            current_path, final_res, final_path);

  return {final_res, final_path};
}

} // namespace BranchAndBoundSolution
