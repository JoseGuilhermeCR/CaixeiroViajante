/**
 * @file main.cc
 * @author José Guilherme de C. Rodrigues
 * @version 1.0
 * @date 2021-05-06
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <BranchAndBoundTSP.hh>
#include <BruteForceTSP.hh>
#include <DynamicTSP.hh>

#include <iostream>
#include <chrono>
#include <iomanip>
#include <thread>
#include <mutex>

std::vector<double> SetBranchAndBound(uint32_t n, const std::vector<double> &xs, const std::vector<double> &ys)
{
    std::vector<BranchAndBoundSolution::Coord> coordinates(n);
    for (uint32_t u = 0; auto &coord : coordinates) {
        coord.x = xs[u];
        coord.y = ys[u];
        ++u;
    }

    std::vector<double> adjacency_matrix(n * n);
    for (uint32_t u = 0; const auto &first : coordinates) {
        for (const auto &second : coordinates) {
            adjacency_matrix[u++] = first.distance(second);
        }
    }

    return adjacency_matrix;
}

std::pair<double, std::vector<uint32_t>> RunBranchAndBound(uint32_t n, const std::vector<double> &adjacency_matrix)
{
    return BranchAndBoundSolution::TravellingSalesPerson(n, adjacency_matrix);
}

void PrintBranchAndBound(const std::pair<double, std::vector<uint32_t>> &resp)
{
    std::cout << "Cost: " << resp.first << '\n' << "Path: ";
    for (uint32_t u = 0; u != resp.second.size() - 1; ++u) {
        std::cout << resp.second[u] + 1 << ' ';
    }
    std::cout << '\n';
}

std::vector<std::vector<double>> SetBruteForce(uint32_t n, const std::vector<double> &xs, const std::vector<double> &ys)
{
    BruteForceSolution::InitCidades(n, xs, ys);

    std::vector<std::vector<double>> matrix(n, std::vector<double>(n));

    BruteForceSolution::InitDist(matrix, n);

    return matrix;
}

std::pair<std::vector<uint32_t>, double> RunBruteForce(const std::vector<std::vector<double>> &matrix)
{
    return BruteForceSolution::TSP(matrix);
}

void PrintBruteForce(const std::pair<std::vector<uint32_t>, double> &resp)
{
    std::cout << "Cost: " << resp.second << '\n' << "Path: ";
    for (auto value : resp.first) {
        std::cout << value + 1 << ' ';
    }
    std::cout << '\n';
}

std::pair<DynamicSolution::Graph, std::vector<uint32_t>> SetDynamic(uint32_t n, const std::vector<double> &xs, const std::vector<double> &ys)
{
    DynamicSolution::Graph graph(n);
    for (uint32_t u = 0; u != n; ++u) {
        graph.AddPos(u, xs[u], ys[u]);
    }

    graph.CreateEdges();

    std::vector<uint32_t> init_sub_set;
    for (uint32_t u = 1; u != n; ++u) {
        init_sub_set.push_back(u);
    }

    return { graph, init_sub_set };
}

std::pair<double, std::vector<uint32_t>> RunDynamic(DynamicSolution::Graph &graph, const std::vector<uint32_t> init_sub_set)
{
    return graph.OPT(0, init_sub_set);
}

void PrintDynamic(const std::pair<double, std::vector<uint32_t>> &resp)
{
    std::cout << "Cost: " << resp.first << '\n' << "Path: ";
    for (uint32_t u = resp.second.size() - 1; static_cast<int32_t>(u) >= 0; --u) {
        std::cout << resp.second.at(u) + 1 << ' ';
    }
    std::cout << '\n';
}

int32_t main()
{
    auto program_start_time = std::chrono::steady_clock::now();

    uint32_t n;
    std::cin >> n;

    std::vector<double> xs(n);
    std::vector<double> ys(n);
    for (uint32_t u = 0; u != n; ++u) {
        std::cin >> xs[u] >> ys[u];
    }

    std::cout << std::fixed << std::setprecision(9);
 
#ifdef THREADED
    std::cout << "Attention: Program compiled with threading enabled." << '\n';
    std::mutex print_mutex;

    auto dyn = std::thread([&n, &xs, &ys, &print_mutex] () {
        auto dyn_graph_init_subset = SetDynamic(n, xs, ys);

        auto dyn_start_time = std::chrono::steady_clock::now();
        auto dyn_res = RunDynamic(dyn_graph_init_subset.first, dyn_graph_init_subset.second);
        auto dyn_end_time = std::chrono::steady_clock::now();

        std::chrono::duration<double> elapsed_dyn_seconds = dyn_end_time - dyn_start_time;

        const std::lock_guard<std::mutex> lock(print_mutex);
        std::cout << "Dynamic Solution executed in " << elapsed_dyn_seconds.count() << " seconds." << '\n';
        std::cout << "===Output===" << '\n';
        PrintDynamic(dyn_res);
        std::cout << "============" << '\n';
    });

    auto bab = std::thread([&n, &xs, &ys, &print_mutex] () {
        auto bab_adjacency_matrix = SetBranchAndBound(n, xs, ys);

        auto bab_start_time = std::chrono::steady_clock::now();
        auto bab_res = RunBranchAndBound(n, bab_adjacency_matrix);
        auto bab_end_time = std::chrono::steady_clock::now();

        std::chrono::duration<double> elapsed_bab_seconds = bab_end_time - bab_start_time;

        const std::lock_guard<std::mutex> lock(print_mutex);
        std::cout << "Branch and Bound Solution executed in " << elapsed_bab_seconds.count() << " seconds." << '\n';
        std::cout << "===Output===" << '\n';
        PrintBranchAndBound(bab_res);
        std::cout << "============" << '\n';
    });

    auto btf = std::thread([&n, &xs, &ys, &print_mutex] () {
        auto btf_adjacency_matrix = SetBruteForce(n, xs, ys);

        auto btf_start_time = std::chrono::steady_clock::now();
        auto btf_res = RunBruteForce(btf_adjacency_matrix);
        auto btf_end_time = std::chrono::steady_clock::now();

        std::chrono::duration<double> elapsed_btf_seconds = btf_end_time - btf_start_time;

        const std::lock_guard<std::mutex> lock(print_mutex);
        std::cout << "Brute Force Solution executed in " << elapsed_btf_seconds.count() << " seconds." << '\n';
        std::cout << "===Output===" << '\n';
        PrintBruteForce(btf_res);
        std::cout << "============" << '\n';
    });

    dyn.join();
    bab.join();
    btf.join();

    auto program_end_time = std::chrono::steady_clock::now();

    std::chrono::duration<double> elapsed_program_seconds = program_end_time - program_start_time;
    std::cout << "Program executed in " << elapsed_program_seconds.count() << " seconds." << '\n';
#else
    // Set everything for every algorithm.
    auto dyn_graph_init_subset = SetDynamic(n, xs, ys);
    auto bab_adjacency_matrix = SetBranchAndBound(n, xs, ys);
    auto btf_adjacency_matrix = SetBruteForce(n, xs, ys);

    // Run and measure execution time.
    auto dyn_start_time = std::chrono::steady_clock::now();
    auto dyn_res = RunDynamic(dyn_graph_init_subset.first, dyn_graph_init_subset.second);
    auto dyn_end_time = std::chrono::steady_clock::now();

    auto bab_start_time = std::chrono::steady_clock::now();
    auto bab_res = RunBranchAndBound(n, bab_adjacency_matrix);
    auto bab_end_time = std::chrono::steady_clock::now();

    auto btf_start_time = std::chrono::steady_clock::now();
    auto btf_res = RunBruteForce(btf_adjacency_matrix);
    auto btf_end_time = std::chrono::steady_clock::now();

    auto program_end_time = std::chrono::steady_clock::now();

    // Calculate execution time.
    auto elapsed_dyn_seconds = dyn_end_time - dyn_start_time;
    auto elapsed_bab_seconds = bab_end_time - bab_start_time;
    auto elapsed_btf_seconds = btf_end_time - btf_start_time;
    auto elapsed_program_seconds = program_end_time - program_start_time;
    
    // From here on, only printing happens.
    std::cout << std::fixed << std::setprecision(9);

    std::cout << "Dynamic Solution executed in " << elapsed_dyn_seconds.count() << " seconds." << '\n';
    std::cout << "===Output===" << '\n';
    PrintDynamic(dyn_res);
    std::cout << "============" << '\n';

    std::cout << "Branch and Bound Solution executed in " << elapsed_bab_seconds.count() << " seconds." << '\n';
    std::cout << "===Output===" << '\n';
    PrintBranchAndBound(bab_res);
    std::cout << "============" << '\n';

    std::cout << "Brute Force Solution executed in " << elapsed_btf_seconds.count() << " seconds." << '\n';
    std::cout << "===Output===" << '\n';
    PrintBruteForce(btf_res);
    std::cout << "============" << '\n';

    std::cout << "Program executed in " << elapsed_program_seconds.count() << " seconds." << '\n';
#endif
    return 0;
}
