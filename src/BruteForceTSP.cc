/**
 * @file BruteForceTSP.cc
 * @author Paulo Lana, refatorado por José Guilherme de C. Rodrigues
 * @version 1.0
 * @date 2021-05-06
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <BruteForceTSP.hh>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <limits>

namespace BruteForceSolution {

static std::vector<Point> cities;

Point::Point() noexcept
    : x()
    , y()
{}

Point::Point(uint32_t _x, uint32_t _y) noexcept
    : x(_x)
    , y(_y)

{}

double Point::distance(const Point &u, const Point &v) noexcept
{
    return std::sqrt((u.x - v.x) * (u.x - v.x) + (u.y - v.y) * (u.y - v.y));
}

void InitCidades(uint32_t n, const std::vector<double> &xs, const std::vector<double> &ys)
{
    cities.clear();    

    for (uint32_t u = 0; u != n; ++u) {
        cities.push_back(Point(xs[u], ys[u]));
    }
}

void InitDist(std::vector<std::vector<double>> &matrix, uint32_t n)
{
    for (uint32_t u = 0; u != n; ++u) {
        for (uint32_t v = 0; v != n; ++v) {
            matrix[u][v] = Point::distance(cities[u], cities[v]);
        }
    }
}

std::pair<std::vector<uint32_t>, double> TSP(const std::vector<std::vector<double>> &matrix)
{
    std::vector<uint32_t> vertex(matrix.size());
    for (uint32_t u = 0; auto &value : vertex) {
        value = u++;
    }
    
    std::vector<uint32_t> aux_vec;

    auto smallest_path = std::numeric_limits<double>::infinity();

    do {
        auto current_path = 0.0;
        uint32_t k = 0;

        for (uint32_t u = 0; u != vertex.size(); ++u) {
            current_path += matrix[k][vertex[u]];
            k = vertex[u];
        }
        current_path += matrix[k][0];

        if (current_path < smallest_path) {
            aux_vec = vertex;
            smallest_path = current_path;
        }
    } while (std::next_permutation(vertex.begin(), vertex.end()));

    return { aux_vec, smallest_path };
}

}
