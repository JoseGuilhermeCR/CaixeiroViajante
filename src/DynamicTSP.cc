/**
 * @file DynamicTSP.cc
 * @author Sandor Borges Scoggin, refatorado por José Guilherme de C. Rodrigues
 * @version 1.0
 * @date 2021-05-06
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <DynamicTSP.hh>
#include <cmath>
#include <limits>
#include <algorithm>

namespace DynamicSolution {

Graph::Graph(uint32_t size)
    : m_size(size)
    , m_matrix(m_size, std::vector<double>(m_size))
    , m_pos(m_size)
    {}

void Graph::AddPos(uint32_t n, double x, double y)
{
    m_pos.at(n).first = x;
    m_pos.at(n).second = y;
}

void Graph::CreateEdges()
{
    for (uint32_t u = 0; u != m_size; ++u) {
        for (uint32_t v = 0; v != m_size; ++v) {
            const auto ux = m_pos.at(u).first;
            const auto uy = m_pos.at(u).second;
            
            const auto vx = m_pos.at(v).first;
            const auto vy = m_pos.at(v).second;

            const double weight = std::sqrt((ux - vx) * (ux - vx) + (uy - vy) * (uy - vy));

            m_matrix.at(u).at(v) = weight;
            m_matrix.at(v).at(u) = weight;
        }
    }
}

std::pair<double, std::vector<uint32_t>> Graph::OPT(uint32_t v, const std::vector<uint32_t> &sub_set)
{
    auto cost = std::numeric_limits<double>::infinity();
    std::vector<uint32_t> vertices;

    if (!sub_set.empty()) {
        for (auto u : sub_set) {
            auto sub_next = sub_set;
            auto pos = std::find(sub_next.begin(), sub_next.end(), u);

            sub_next.erase(pos);

            auto opt = OPT(u, sub_next);
            auto rec = m_matrix.at(u).at(v) + opt.first;
            if (rec < cost) {
                cost = rec;
                vertices = opt.second;
            }
        }
    } else {
        cost = m_matrix.at(v).at(0);
    }

    vertices.push_back(v);

    return { cost, vertices };
}

}
