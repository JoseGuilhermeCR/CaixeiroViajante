A maneira fácil de executar é rodando os testes diretamente:
./tests.sh

Caso queira compilar manualmente...
Suportamos dois tipos de compilação. 
O programa pode executar os algoritmos sequencialmente e demorar muito mais tempo
ou executar cada um em seu próprio thread para adiantar o processo.

Para compilar normalmente:
make
Para compilar com threads:
make DEFS=THREADED

Para executar basta digitar
./tsp.out

A seguir, basta dar a entrada para o programa e pronto.

Para limpar e compilar novamente:
make clean

